﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pagination : MonoBehaviour
{
    public GameObject paginationPrefab;   
    public GameObject paginationParent; 
	public Color colorNormal;
	public Color colorSelected;
	public static Pagination SP;
	void Awake(){
		SP = this;
	}
    
	[HideInInspector]
	public List<GameObject> loadedPagination = new List<GameObject>();
    public void LoadPagination(){
		int i =0;
		while(i < UISlideShow.SP.slideSource.transform.childCount) {
            var pagination = Instantiate(paginationPrefab, Vector3.zero, Quaternion.identity);
            pagination.transform.SetParent(paginationParent.transform, false);
            pagination.GetComponent<PaginationPrefabs>().myIndex = i;
            pagination.GetComponent<PaginationPrefabs>().LoadMyValue();
            loadedPagination.Add(pagination);
            i++;
        }
    }
	public void LoadCurrentSelected(int index){
		foreach(var tog in loadedPagination){
			tog.GetComponentInChildren<Image>().color = colorNormal;
		}
		loadedPagination[index].GetComponentInChildren<Image>().color = colorSelected;
	}
}
