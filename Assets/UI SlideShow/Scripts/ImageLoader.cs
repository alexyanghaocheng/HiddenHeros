﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public enum CaptionType
{
	Top = 0,
	Bottom = 1,

}
public class ImageLoader : MonoBehaviour {

	public GameObject imagePrefab;
	public GameObject slideSource;
	public List<ImagesDetails> imagesDetail = new List<ImagesDetails>();
	public static ImageLoader SP;
	void Awake(){
		SP = this;
	}
	void Start () {
		if(UISlideShow.SP && UISlideShow.SP.LoadingImagesAtRuntime)
		StartCoroutine(CreateSlideShow());
	}
	[Serializable]
	public class ImagesDetails{
		public Sprite slideImages;
		public string caption;
		public CaptionType captionAlignment;
	}

	[HideInInspector]
	public List<GameObject> loadedSlides = new List<GameObject>();

	IEnumerator CreateSlideShow() {
		yield return null;
		int i =0;
		while(i < imagesDetail.Count) {
			GameObject obj = Instantiate(imagePrefab);
			obj.GetComponent<RectTransform>().localPosition = Vector3.zero;
			obj.GetComponent<Image>().sprite = imagesDetail[i].slideImages;
			obj.GetComponent<SelectImage>().captionForThisSlide = imagesDetail[i].caption;
			obj.GetComponent<SelectImage>().captionAlignment = imagesDetail[i].captionAlignment;
			obj.GetComponent<SelectImage>().myId = i;
			obj.transform.SetParent(slideSource.transform,false);
			loadedSlides.Add(obj);
			i++;
		}
		Debug.Log(1f/imagesDetail.Count);
		UISlideShow.SP.InitialStartSlide();
	}
}

