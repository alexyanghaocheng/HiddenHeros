﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PaginationPrefabs : MonoBehaviour
{
    public int myIndex;
    public Text paginationValue;

    public void LoadMyValue(){
        paginationValue.text = (myIndex+1).ToString();
    }

    public void OnClick(){
        if(UISlideShow.SP){
		    // Debug.Log(myIndex+"Index");
            UISlideShow.SP.LoadMyShow(myIndex);
        }
    }
}
