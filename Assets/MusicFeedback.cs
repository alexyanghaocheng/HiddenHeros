using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicFeedback : MonoBehaviour
{
    public AudioClip clip;
    public AudioSource targetAudioSource;
    [Range(0, 1)]
    public float volume = 1;

    /// <summary>
    /// Switch the music to play. 
    /// </summary>
    public void PlayMusic()
    {
        targetAudioSource.Stop();
        targetAudioSource.volume = this.volume;
        targetAudioSource.clip = clip;
        targetAudioSource.Play();
    }
}
