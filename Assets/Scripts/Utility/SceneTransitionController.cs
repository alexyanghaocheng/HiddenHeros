using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class SceneTransitionController : SingletonMonoBehavior<SceneTransitionController>
{
    [SerializeField] private Animator animator;

    public void TransitionAndLoadScene(int index)
    {
        StartCoroutine(FadeOut(index));
    }

    public void WaitAndReloadCurrentScene()
    {
        StartCoroutine(WaitThenFadeOut(SceneManager.GetActiveScene().buildIndex));
    }

    private IEnumerator FadeOut(int index)
    {
        animator.SetTrigger("FadeOut");
        yield return new WaitForSecondsRealtime(0.5f);
        SceneManager.LoadScene(index);
    }

    private IEnumerator WaitThenFadeOut(int index)
    {
        yield return new WaitForSecondsRealtime(3f);
        StartCoroutine(FadeOut(index));
    }
}
