using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class LevelLoader : MonoBehaviour
{
    public int SceneToLoadIndex;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            SceneTransitionController.Instance.TransitionAndLoadScene(SceneToLoadIndex);
    }
}
