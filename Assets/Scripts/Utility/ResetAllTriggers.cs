using System.Linq;
using UnityEngine;

public class ResetAllTriggers : StateMachineBehaviour
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        foreach (var parameter in animator.parameters.Where(parameter => parameter.type == AnimatorControllerParameterType.Trigger))
        {
            animator.ResetTrigger(parameter.name);
        }
    }
}
