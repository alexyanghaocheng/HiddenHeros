using System.Collections;
using System.Collections.Generic;
using FIMSpace.FLook;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour
{
    [SerializeField] private FLookAnimator _fLookAnimator;

    // Update is called once per frame
    void Update()
    {
        _fLookAnimator.SetLookTarget(PlayerController.Instance.cameraRootTransform);
    }
}
