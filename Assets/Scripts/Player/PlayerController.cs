using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;
using UnityEngine.Serialization;

public class PlayerController : SingletonMonoBehavior<PlayerController>
{
    public Transform cameraRootTransform;
    
    private Animator _animator;
    private PlayerInput _playerInput;
    private bool _hasDied;
    private bool canDie;
    public event Action ComboDamageFrameStart;
    public event Action ComboDamageFrameEnd;
    

    //The number of lights the player is lit by. 
    public int litByLightCount;

    public bool IsLit()
    {
        return litByLightCount > 0;
    }
    

    private void Start()
    {
        _animator = GetComponentInChildren<Animator>();
        _playerInput = GetComponentInChildren<PlayerInput>();
        canDie = true;
    }

    private void Update()
    {
       // if(Input.GetKeyDown(KeyCode.P))
       //     Die();
    }

    /// <summary>
    /// Kill you first time dying. 
    /// </summary>
    public void Die()
    {
        if (_hasDied || !canDie) return;

        _hasDied = true;
        _animator.SetTrigger("Die");
        _playerInput.DeactivateInput();
        SceneTransitionController.Instance.WaitAndReloadCurrentScene();
    }

    public void TurnOnInvincibility()
    {
        canDie = false;
    }

    public void TurnOffInvincibility()
    {
        canDie = true;
    }

    protected virtual void OnComboDamageFrameStart()
    {
        ComboDamageFrameStart?.Invoke();
    }

    protected virtual void OnComboDamageFrameEnd()
    {
        ComboDamageFrameEnd?.Invoke();
    }

    public void DeactivatePlayerInput()
    {
        _playerInput.DeactivateInput();
    }

    public void ActivatePlayerInput()
    {
        _playerInput.ActivateInput();
    }

    public bool AttackOneTriggered()
    {
        return _playerInput.actions["Attack1"].triggered;
    }

    public bool AttackTwoTriggered()
    {
        return _playerInput.actions["Attack2"].triggered;
    }
}
