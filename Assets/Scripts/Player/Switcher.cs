using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Switcher : MonoBehaviour
{
    [SerializeField] private GameObject ToSwitch;
    [SerializeField] private GameObject SwitchTo;

    public void SwitchPlayer()
    {
        Destroy(PlayerController.Instance.gameObject);
        PlayerController.Instance.Nullify();
        SwitchTo.SetActive(true); 
    }

    public void SwitchGameObject()
    {
        ToSwitch.SetActive(false);
        SwitchTo.SetActive(true);
    }
}
