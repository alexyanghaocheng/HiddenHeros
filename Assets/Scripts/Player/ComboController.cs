using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class ComboController : MonoBehaviour
{
    private Animator _animator;

    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (PlayerController.Instance.AttackOneTriggered())
            AttackOne();
        if (PlayerController.Instance.AttackTwoTriggered())
            AttackTwo();
        if(Keyboard.current.anyKey.isPressed)
            _animator.SetTrigger("AnimationCancel");

    }

    private void AttackTwo()
    {
        var animatorStateInfo = _animator.GetCurrentAnimatorStateInfo(1);
        if (animatorStateInfo.IsName("New State")) _animator.SetTrigger("RightAttack1"); 
    }


    /// <summary>
    /// It prevents continuous clicking by resetting triggers every state enter.
    /// </summary>
    private void AttackOne()
    {
        //Get animatorStateInfo. A struct, therefore we can pass by value. Doesn't update for the click. 
        var animatorStateInfo = _animator.GetCurrentAnimatorStateInfo(0);

        //transition into first combo in chain 
        if (animatorStateInfo.IsName("Idle Walk Run Blend")) _animator.SetTrigger("LeftAttack1");
        
        //If the current animation is almost done
        if (animatorStateInfo.normalizedTime > 0.2f)
        {
            //if is combo2, transition to combo2. 
            if (animatorStateInfo.IsName("LeftAttack1")) _animator.SetTrigger("LeftAttack2");

            if (animatorStateInfo.IsName("LeftAttack2")) _animator.SetTrigger("LeftAttack3");

            if (animatorStateInfo.IsName("LeftAttack3")) _animator.SetTrigger("LeftAttack4");
        }
        
    }
    
}
