using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideShowTrigger : MonoBehaviour
{
    [SerializeField] protected int index;
    protected bool hasTriggered;
    
    private void OnTriggerStay(Collider other)
    {
        TriggerSlideShow(other);
    }

    protected virtual void TriggerSlideShow(Collider other)
    {
        if (other.CompareTag("Player") && !hasTriggered)
        {
            hasTriggered = true;
            SlideShowController.Instance.EnableSlideShow(index);
        }     
    }

    public void TriggerSlideShow()
    {
        hasTriggered = true;
        SlideShowController.Instance.EnableSlideShow(index);
    }
}
