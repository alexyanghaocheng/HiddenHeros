using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class SlideShowController : SingletonMonoBehavior<SlideShowController>
{
    public bool SlidesEnabled;
    [SerializeField] private List<GameObject> slideShows;

    // Update is called once per frame
    void Update()
    {
        if (SlidesEnabled)
        {
            Cursor.lockState = CursorLockMode.None;  
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    public void EnableSlideShow(int index)
    {
        PlayerController.Instance.TurnOnInvincibility();
        PlayerController.Instance.DeactivatePlayerInput();
        slideShows[index].SetActive(true);
        SlidesEnabled = true;
    }
}
