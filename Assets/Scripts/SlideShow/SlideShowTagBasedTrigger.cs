using System;
using UnityEngine;

public class SlideShowTagBasedTrigger : SlideShowTrigger
{
    [SerializeField] private String tag;
    
    private void OnTriggerStay(Collider other)
    {
        TriggerSlideShow(other);
    }

    protected override void TriggerSlideShow(Collider other)
    {
        if (other.CompareTag(tag) && !hasTriggered)
        {
            hasTriggered = true;
            SlideShowController.Instance.EnableSlideShow(index);
        } 
    }
}
