using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.Serialization;

[RequireComponent(typeof(Light))]
[RequireComponent(typeof(SphereCollider))]
public class LightController : MonoBehaviour
{
    [SerializeField] private Light lightSource;
    [SerializeField] private SphereCollider collider;
    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, lightSource.range);
    }

    private void Awake()
    {
        collider.radius = lightSource.range;
        collider.isTrigger = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerController.Instance.litByLightCount++;
        } 
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerController.Instance.litByLightCount--;
        }  
    }
}
