using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime.Tasks.Unity.UnityGameObject;
using FIMSpace.FLook;
using UnityEngine;

public class EnemyConditional : Conditional
{
    protected FLookAnimator _fLookAnimator;

    public override void OnAwake()
    {
        _fLookAnimator = gameObject.GetComponentInChildren<FLookAnimator>();
    }

}
