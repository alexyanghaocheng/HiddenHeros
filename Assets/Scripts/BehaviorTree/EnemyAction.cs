using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAction : Action
{
    protected Animator Animator;
    protected NavMeshAgent NavAgent;
    
    public override void OnAwake()
    {
        Animator = gameObject.GetComponentInChildren<Animator>();
        NavAgent = gameObject.GetComponentInChildren<NavMeshAgent>();
    }
}
