using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public class IsInAttackRange : EnemyConditional
{
    public float AttackRange;

    public override TaskStatus OnUpdate()
    {
        if (Physics.CheckSphere(transform.position, AttackRange, (LayerMask)GlobalVariables.Instance.GetVariable("PlayerLayerMask").GetValue()))
        {
            return TaskStatus.Success;
        } else
        {
            return TaskStatus.Failure;
        }
    }
}
