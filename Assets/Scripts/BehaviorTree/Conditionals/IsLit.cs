using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public class IsLit : EnemyConditional
{
    
    public override TaskStatus OnUpdate()
    {
        if (PlayerController.Instance.IsLit())
        {
            return TaskStatus.Success;
        }
        else
        {
            return TaskStatus.Failure;
        }
    }
}
