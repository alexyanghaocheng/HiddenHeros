using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public class IsInSight : EnemyConditional
{
    public float SightRange;

    public override TaskStatus OnUpdate()
    {
        if (Physics.CheckSphere(transform.position, SightRange, (LayerMask)GlobalVariables.Instance.GetVariable("PlayerLayerMask").GetValue()))
        {
            //set wolf to look at player
            _fLookAnimator.SetLookTarget(PlayerController.Instance.transform);
            return TaskStatus.Success;
        } else
        {
            //set wolf to not look at player
            _fLookAnimator.SetLookTarget(null);
            return TaskStatus.Failure;
        }
        
    }
}
