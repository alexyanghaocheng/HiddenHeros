using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public class NotCloseEnough : EnemyConditional
{
    public float Range;

    public override TaskStatus OnUpdate()
    {
        if (Physics.CheckSphere(transform.position, Range, (LayerMask)GlobalVariables.Instance.GetVariable("PlayerLayerMask").GetValue()))
        {
            return TaskStatus.Failure;
        } else
        {
            return TaskStatus.Success;
        }
        
    }
}
