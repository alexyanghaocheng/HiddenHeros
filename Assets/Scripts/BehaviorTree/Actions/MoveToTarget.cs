using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class MoveToTarget : EnemyAction
{
    private Transform _targetTransform;
    
    /// <summary>
    /// Will chase player, since it has caught eye on player.
    /// </summary>
    public override void OnStart()
    {
        _targetTransform = PlayerController.Instance.gameObject.transform;
        NavAgent.SetDestination(_targetTransform.position);
        NavAgent.isStopped = false;
        //do I need to set animator manually? Depends on Third Person Controller.
    }

    public override TaskStatus OnUpdate()
    {
        
        //Return failure if it cannot find player
        if (_targetTransform == null)
            return TaskStatus.Failure;
        
        //If path complete, keep moving toward player. Gives it its intelligent behavior of changing paths.
        if (NavAgent.pathStatus == NavMeshPathStatus.PathComplete)
        {
            NavAgent.SetDestination(_targetTransform.position);
            NavAgent.isStopped = false;
        }

        if (NavAgent.pathStatus == NavMeshPathStatus.PathPartial)
        {
            return TaskStatus.Failure;
        }

        //When you are in range, you can stop. 
        if (Vector3.Distance(transform.position, _targetTransform.position) < 1f)
        {
            NavAgent.isStopped = true;
            return TaskStatus.Success;
        }

        //if character is lit, this node returns back failed. 
        if (PlayerController.Instance.IsLit())
        {
            NavAgent.isStopped = true;
            return TaskStatus.Failure;
        }
        
        return TaskStatus.Running;

    }
}
