using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public class Attack : EnemyAction
{
    public override void OnStart()
    {
        Animator.SetTrigger("Attack");
        PlayerController.Instance.Die();
    }

    //already killed the player. So Success. 
    public override TaskStatus OnUpdate()
    {
        return TaskStatus.Success;
    }
}