using BehaviorDesigner.Runtime;
using FIMSpace.FLook;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;


public class Wolf : MonoBehaviour, IDamagable
{
    [SerializeField] private Animator _animator;
    [SerializeField] private BehaviorTree _behaviorTree;
    [SerializeField] private NavMeshAgent _navMeshAgent;
    [SerializeField] private FLookAnimator _fLookAnimator;
    private bool hasDied;
    
    public UnityEvent onDamage;

    public void Damage(Vector3 contactPoint = new Vector3())
    {
        if (hasDied)
            return;

        hasDied = true;
        onDamage.Invoke();
        _animator.SetTrigger("Die");
        _behaviorTree.enabled = false;
        _navMeshAgent.enabled = false;
        _fLookAnimator.enabled = false;
    }
}
