using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MusicTrigger : MonoBehaviour
{
    private bool hasTriggered;
    public UnityEvent onEnterTrigger;
    
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && !hasTriggered)
        {
            hasTriggered = true;
            onEnterTrigger.Invoke();
        }    
    }
    
    
}
