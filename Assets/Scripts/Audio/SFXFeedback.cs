using System.Collections;
using UnityEngine;

public class SFXFeedback : MonoBehaviour
{
    public AudioClip clip;
    public AudioClip[] randomClipList;
    public AudioSource targetAudioSource;
    [Range(0, 1)]
    public float volume = 1;

    private Coroutine _playRoutine;
    
    public void PlayClip()
    {
        if (clip == null)
            return;
        targetAudioSource.volume = this.volume;
        targetAudioSource.PlayOneShot(clip);
    }

    public void PlayRandomClipInList()
    {
        if (randomClipList == null || randomClipList.Length == 0)
            return;
        targetAudioSource.volume = this.volume;
        targetAudioSource.PlayOneShot(randomClipList[Random.Range(0, randomClipList.Length)]);
    }

    public void PlayRandomWithCoolDown()
    {
        if (_playRoutine == null)
        {
            _playRoutine = StartCoroutine(PlayAndWait(1f));
        }
    }

    private IEnumerator PlayAndWait(float seconds)
    {
        PlayRandomClipInList();
        yield return new WaitForSeconds(seconds);
    }

    public void PlaySpecificClip(AudioClip clipToPlay = null)
    {
        if (clipToPlay == null)
            clipToPlay = clip;
        if (clipToPlay == null)
            return;
        targetAudioSource.volume = this.volume;
        targetAudioSource.PlayOneShot(clipToPlay);
    }
}