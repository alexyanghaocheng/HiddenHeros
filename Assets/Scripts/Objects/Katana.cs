using System;
using UnityEngine;

public class Katana : MonoBehaviour
{
    private Collider trigger;
    [SerializeField] private Transform forcePoint;

    private void Awake()
    {
        trigger = GetComponent<Collider>();
        trigger.enabled = false;
    }

    private void Start()
    {
        PlayerController.Instance.ComboDamageFrameStart += EnableTrigger;
        PlayerController.Instance.ComboDamageFrameEnd += DisableTrigger;
    }

    private void OnDisable()
    {
        PlayerController.Instance.ComboDamageFrameStart -= EnableTrigger;
        PlayerController.Instance.ComboDamageFrameEnd -= DisableTrigger;
    }

    private void OnTriggerEnter(Collider other)
    {
        Damage(other);
    }

    private void OnTriggerExit(Collider other)
    {
        Damage(other);
    }

    private void Damage(Collider other)
    {
        // Does not allocate new memory when said object does not exist
        if (other.gameObject.TryGetComponent<IDamagable>(out IDamagable damagable))
        {
            damagable.Damage(forcePoint.position);
        }  
    }

    public void EnableTrigger()
    {
        trigger.enabled = true;
    }

    public void DisableTrigger()
    {
        trigger.enabled = false;
    }
}
