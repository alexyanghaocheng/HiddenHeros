using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityEngine.Serialization;

public class Bamboo : MonoBehaviour, IDamagable
{
    [SerializeField] private NavMeshObstacle _navMeshObstacle;
    [SerializeField] private Rigidbody rb;
    public UnityEvent onDamage;
    
    /// <summary>
    /// Add explosion at contact point.
    /// </summary>
    /// <param name="contactPoint"></param>
    public void Damage(Vector3 contactPoint)
    {
        if(_navMeshObstacle)
            _navMeshObstacle.enabled = false;
        rb.isKinematic = false;
        rb.AddExplosionForce(1000f, contactPoint, 1f);
        onDamage.Invoke();
    }
}

